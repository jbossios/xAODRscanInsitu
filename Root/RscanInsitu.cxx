#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthEventContainer.h" // For new cut 
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include <xAODJet/JetContainer.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/tools/Message.h"
#include "AthContainers/ConstDataVector.h"
#include <xAODRscanInsitu/RscanInsitu.h>
#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/tools/ReturnCheck.h>
#include "TFile.h"
#include "TAxis.h"
#include "TSystem.h"

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
   do {                                                     \
      if( ! EXP.isSuccess() ) {                             \
         Error( CONTEXT,                                    \
                XAOD_MESSAGE( "Failed to execute: %s" ),    \
                #EXP );                                     \
         return EL::StatusCode::FAILURE;                    \
      }                                                     \
   } while( false )

// this is needed to distribute the algorithm to the workers
ClassImp(RscanInsitu)

RscanInsitu :: RscanInsitu () :
 m_treeStream("tree")
{

  m_debug = false;
  m_isMC  = true;
  m_treeCreated = false;
 
}

EL::StatusCode RscanInsitu :: setupJob (EL::Job& job)
{

  job.useXAOD();

  // let's initialize the algorithm to use the xAODRootAccess package
  EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
  
  EL::OutputStream outForTree( m_treeStream );
  job.outputAdd (outForTree);
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode RscanInsitu::initialize() { 

  Info("RscanInsitu()", "Calling initialize \n");

  // get TEvent and TStore
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();

  // get user options
  Info("RscanInsitu::setupJob()", "User configuration read from : %s", m_configName.c_str());
  m_configName = gSystem->ExpandPathName( m_configName.c_str() );
  TEnv* config = new TEnv(m_configName.c_str());
  if( !config ) {
    Error("RscanInsitu::setupJob()", "Failed to read config file!");
    Error("RscanInsitu::setupJob()", "config name : %s",m_configName.c_str());
    return EL::StatusCode::FAILURE;
  }

  m_debug              = config->GetValue("Debug", false);
  m_saveRscanTruthJets = config->GetValue("SaveRscanTruthJets", true);
  m_eventDetailStr     = config->GetValue("EventDetailStr", "pileup shapeEM");
  m_trigDetailStr      = config->GetValue("TrigDetailStr", "basic");
  m_jetDetailStr       = config->GetValue("JetDetailStr", "kinematic rapidity energy layer trackAll clean");
  m_jetDetailStrSyst   = config->GetValue("JetDetailStrSyst", "kinematic clean energy");
  m_inputAlgo          = config->GetValue("InputAlgo",       "");

  // show user options
  Info("RscanInsitu::initialize()", "m_debug = %i", m_debug);
  Info("RscanInsitu::initialize()", "m_saveRscanTruthJets = %i", m_saveRscanTruthJets);
  Info("RscanInsitu::initialize()", "m_eventDetailStr = %s", m_eventDetailStr.c_str());
  Info("RscanInsitu::initialize()", "m_trigDetailStr = %s", m_trigDetailStr.c_str());
  Info("RscanInsitu::initialize()", "m_jetDetailStr  = %s", m_jetDetailStr.c_str());
  Info("RscanInsitu::initialize()", "m_jetDetailStrSyst  = %s", m_jetDetailStrSyst.c_str());
  Info("RscanInsitu::initialize()", "m_inputAlgo  = %s", m_inputAlgo.c_str());

  // count number of events
  m_eventCounter   = 0;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode RscanInsitu :: fileExecute ()
{

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode RscanInsitu :: changeInput (bool /*firstFile*/)
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode RscanInsitu :: histInitialize ()
{

  Info("RscanInsitu()", "Calling histInitialize \n");
 
  return EL::StatusCode::SUCCESS;

}

void RscanInsitu::AddTree(std::string name) {
  
  std::string treeName("outTree");
  treeName += name; // add systematic
  TTree * outTree = new TTree(treeName.c_str(),treeName.c_str());
  if( !outTree ) {
    Error("execute()","Failed to get output tree!");
  }
  TFile* treeFile = wk()->getOutputFile( m_treeStream );
  outTree->SetDirectory( treeFile );

  MiniTree* miniTree = new MiniTree(m_event, outTree, treeFile);
  miniTree->AddEvent( m_eventDetailStr );
  miniTree->AddTrigger( m_trigDetailStr);
  miniTree->AddJets( m_jetDetailStr, "RefJet" ); 
  if(m_jetColl=="All" || m_jetColl.Contains("2LC")) miniTree->AddJets( m_jetDetailStr, "2LCJet" ); 
  if(m_jetColl=="All" || m_jetColl.Contains("3LC")) miniTree->AddJets( m_jetDetailStr, "3LCJet" ); 
  if(m_jetColl=="All" || m_jetColl.Contains("5LC")) miniTree->AddJets( m_jetDetailStr, "5LCJet" ); 
  if(m_jetColl=="All" || m_jetColl.Contains("6LC")) miniTree->AddJets( m_jetDetailStr, "6LCJet" ); 
  if(m_jetColl=="All" || m_jetColl.Contains("7LC")) miniTree->AddJets( m_jetDetailStr, "7LCJet" ); 
  if(m_jetColl=="All" || m_jetColl.Contains("8LC")) miniTree->AddJets( m_jetDetailStr, "8LCJet" ); 
  if(m_isMC){
    miniTree->AddJets( m_jetDetailStr, "TruthRefJet" ); 
    if(m_saveRscanTruthJets){
      if(m_jetColl=="All" || m_jetColl.Contains("2LC")) miniTree->AddJets( m_jetDetailStr, "Truth2LCJet" ); 
      if(m_jetColl=="All" || m_jetColl.Contains("3LC")) miniTree->AddJets( m_jetDetailStr, "Truth3LCJet" ); 
      if(m_jetColl=="All" || m_jetColl.Contains("5LC")) miniTree->AddJets( m_jetDetailStr, "Truth5LCJet" ); 
      if(m_jetColl=="All" || m_jetColl.Contains("6LC")) miniTree->AddJets( m_jetDetailStr, "Truth6LCJet" ); 
      if(m_jetColl=="All" || m_jetColl.Contains("7LC")) miniTree->AddJets( m_jetDetailStr, "Truth7LCJet" ); 
      if(m_jetColl=="All" || m_jetColl.Contains("8LC")) miniTree->AddJets( m_jetDetailStr, "Truth8LCJet" ); 
    }
  }

  m_myTree = miniTree;

  m_treeCreated = true;

  //wk()->addOutput( outTree ); // Don't add this!!!

}

EL::StatusCode RscanInsitu::execute() {

  if(m_debug) Info("RscanInsitu()", "Calling execute \n");

  ++m_eventCounter; //Event that pass the event selection

  //---------------------------
  // Display Event Statistics
  //--------------------------- 
  if (m_eventCounter % 1000 == 0 && m_debug){
        Info("RscanInsitu::execute()", "Event = %i", m_eventCounter);
  }

  //---------------------------
  // Event information
  //--------------------------- 
  if(m_debug) std::cout << "Getting EventInfo container" << std::endl;

  const xAOD::EventInfo* eventInfo(nullptr);
  RETURN_CHECK("RscanInsitu::initialize()", HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, 0, m_debug), "Failed to retrive EventInfo container");
  m_isMC = ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) ? true : false;
  
  //-----------------------------------------
  // Saving event information into IEventInfo
  //-----------------------------------------
  unsigned int run = -1;
  if (!(m_isMC)){ // For Data
    run = eventInfo->runNumber();
  }
  else{
    run = eventInfo->mcChannelNumber();
  }
 
  m_IEventInfo = new IEventInfo();

  m_IEventInfo->SetRunNumber( run );
  m_IEventInfo->SetEventNumber( eventInfo->eventNumber() );
  m_IEventInfo->Setbcid( eventInfo->bcid() );
  m_IEventInfo->SetaverageIntPerXing( eventInfo->averageInteractionsPerCrossing() ) ;
  if(m_isMC) m_IEventInfo->Setmcevt_weight( eventInfo->mcEventWeight() );

  if(m_debug){
    std::cout << "m_IEventInfo->GetRunNumber() = " << m_IEventInfo->GetRunNumber() << std::endl;
    std::cout << "m_IEventInfo->GetEventNumber() = " << m_IEventInfo->GetEventNumber() << std::endl;
    std::cout << "m_IEventInfo->Getbcid() = " << m_IEventInfo->Getbcid() << std::endl;
    std::cout << "m_IEventInfo->GetaverageINtPerXing() = " << m_IEventInfo->GetaverageIntPerXing() << std::endl;
    if(m_isMC) std::cout << "m_IEventInfo->Getmcevt_weight() = " << m_IEventInfo->Getmcevt_weight() << std::endl;
  }

  //-------------------------
  // Getting Jet Containers
  //-------------------------

  // Jet Containers
  std::string inRefContainerName("AntiKt4EMTopoJets_Signal"); // FIXME Temporary should be read in config
  std::string inTruthRefContainerName("AntiKt4TruthJets_Signal"); // FIXME Temporary should be read in config
  std::vector<std::string> inRscanContainerName;
  std::vector<std::string> inTruthRscanContainerName;
  if(m_jetColl=="All"){
    inRscanContainerName.push_back("AntiKt2LCTopoJets_Signal");
    inRscanContainerName.push_back("AntiKt3LCTopoJets_Signal");
    inRscanContainerName.push_back("AntiKt5LCTopoJets_Signal");
    inRscanContainerName.push_back("AntiKt6LCTopoJets_Signal");
    inRscanContainerName.push_back("AntiKt7LCTopoJets_Signal");
    inRscanContainerName.push_back("AntiKt8LCTopoJets_Signal");
    inTruthRscanContainerName.push_back("AntiKt2TruthJets_Signal");
    inTruthRscanContainerName.push_back("AntiKt3TruthJets_Signal");
    inTruthRscanContainerName.push_back("AntiKt5TruthJets_Signal");
    inTruthRscanContainerName.push_back("AntiKt6TruthJets_Signal");
    inTruthRscanContainerName.push_back("AntiKt7TruthJets_Signal");
    inTruthRscanContainerName.push_back("AntiKt8TruthJets_Signal");
  }
  else{
    if(m_jetColl.Contains("2LC")){
      inRscanContainerName.push_back("AntiKt2LCTopoJets_Signal");
      inTruthRscanContainerName.push_back("AntiKt2TruthJets_Signal");
    }
    if(m_jetColl.Contains("3LC")){
      inRscanContainerName.push_back("AntiKt3LCTopoJets_Signal");
      inTruthRscanContainerName.push_back("AntiKt3TruthJets_Signal");
    }
    if(m_jetColl.Contains("5LC")){
      inRscanContainerName.push_back("AntiKt5LCTopoJets_Signal");
      inTruthRscanContainerName.push_back("AntiKt5TruthJets_Signal");
    }
    if(m_jetColl.Contains("6LC")){
      inRscanContainerName.push_back("AntiKt6LCTopoJets_Signal");
      inTruthRscanContainerName.push_back("AntiKt6TruthJets_Signal");
    }
    if(m_jetColl.Contains("7LC")){
      inRscanContainerName.push_back("AntiKt7LCTopoJets_Signal");
      inTruthRscanContainerName.push_back("AntiKt7TruthJets_Signal");
    }
    if(m_jetColl.Contains("8LC")){
      inRscanContainerName.push_back("AntiKt8LCTopoJets_Signal");
      inTruthRscanContainerName.push_back("AntiKt8TruthJets_Signal");
    }
  }

  int nRscan = inRscanContainerName.size();

  // Getting containers

  if(m_debug) std::cout << "Getting " << inRefContainerName << " container" << std::endl;
  const xAOD::JetContainer* RefJets = 0;
  RETURN_CHECK("RscanInsitu::execute()", HelperFunctions::retrieve(RefJets, inRefContainerName, 0, m_store, m_debug), "Failed to retrieve AntiKt4EMTopoJets_Signal container");

  const xAOD::JetContainer* TruthRefJets = 0;
  if(m_isMC){
    if(m_debug) std::cout << "Getting " << inTruthRefContainerName << " container" << std::endl;
    RETURN_CHECK("RscanInsitu::execute()", HelperFunctions::retrieve(TruthRefJets, inTruthRefContainerName, 0, m_store, m_debug), "Failed to retrieve AntiKt4TruthWZJets_Signal container");
  }

  std::vector<const xAOD::JetContainer*> RscanJets;
  const xAOD::JetContainer* temp;
  for(int i=0;i<nRscan;++i){
    if(m_debug) std::cout << "Getting " << inRscanContainerName.at(i) << " container" << std::endl;
    temp = 0;
    RETURN_CHECK("RscanInsitu::execute()", HelperFunctions::retrieve(temp, inRscanContainerName.at(i), 0, m_store, m_debug), "Failed to retrieve Reco Rscan jets container");
    RscanJets.push_back(temp);
  }

  std::vector<const xAOD::JetContainer*> TruthRscanJets;
  if(m_isMC && m_saveRscanTruthJets){
    for(int i=0;i<nRscan;++i){
      if(m_debug) std::cout << "Getting " << inTruthRscanContainerName.at(i) << " container" << std::endl;
      temp = 0;
      RETURN_CHECK("RscanInsitu::execute()", HelperFunctions::retrieve(temp, inTruthRscanContainerName.at(i), 0, m_store, m_debug), "Failed to retrieve Truth Rscan jets container");
      TruthRscanJets.push_back(temp);
    }
  }

  //---------------------------------------
  // Decorating Containers with Jet Scales
  //--------------------------------------- 

  for(auto iJet : *RefJets){
    // ConstitScale
    if(m_debug) std::cout << "Retrieving JetConstitScaleMomentum" << std::endl;
    xAOD::JetFourMom_t jetConstitScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum");
    iJet->auxdecor< float >( "constScaleEta") = jetConstitScaleP4.eta();
    // PileupScale
    if(m_debug) std::cout << "Retrieving JetPileupScaleMomentum" << std::endl;
    xAOD::JetFourMom_t jetPileupScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetPileupScaleMomentum");
    iJet->auxdecor< float >( "pileupScaleEta") = jetPileupScaleP4.eta();
    // OriginConstitScale
    if(m_debug) std::cout << "Retrieving JetOriginConstitScaleMomentum" << std::endl;
    xAOD::JetFourMom_t jetOriginConstitScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetOriginConstitScaleMomentum");
    iJet->auxdecor< float >( "originConstitScaleEta") = jetOriginConstitScaleP4.eta();
    // EtaJESScale
    if(m_debug) std::cout << "Retrieving JetEtaJESScaleMomentum" << std::endl;
    xAOD::JetFourMom_t jetEtaJESScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetEtaJESScaleMomentum");
    iJet->auxdecor< float >( "etaJESScaleEta") = jetEtaJESScaleP4.eta();
    // GSCScale
    if(m_debug) std::cout << "Retrieving JetGSCScaleMomentum" << std::endl;
    xAOD::JetFourMom_t jetGSCScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetGSCScaleMomentum");
    iJet->auxdecor< float >( "gscScaleEta") = jetGSCScaleP4.eta();
    // InsituScale
    if(!m_isMC){
      if(m_debug) std::cout << "Retrieving JetInsituScaleMomentum" << std::endl;
      xAOD::JetFourMom_t jetInsituScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetInsituScaleMomentum");
      iJet->auxdecor< float >( "insituScaleEta") = jetInsituScaleP4.eta();
    }
  }

  /*
  for(int i=0;i<nRscan;++i){

    for(auto iJet : *RscanJets[i]){
      // ConstitScale
      if(m_debug) std::cout << "Retrieving JetConstitScaleMomentum" << std::endl;
      xAOD::JetFourMom_t jetConstitScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum");
      iJet->auxdecor< float >( "constitScalepT") = jetConstitScaleP4.pt()*0.001;
      iJet->auxdecor< float >( "constitScaleEta") = jetConstitScaleP4.eta();
      iJet->auxdecor< float >( "constitScalePhi") = jetConstitScaleP4.phi();
      iJet->auxdecor< float >( "constitScaleE") = jetConstitScaleP4.e()*0.001;
      // PileupScale
      if(m_debug) std::cout << "Retrieving JetPileupScaleMomentum" << std::endl;
      xAOD::JetFourMom_t jetPileupScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetPileupScaleMomentum");
      iJet->auxdecor< float >( "pileupScalepT") = jetPileupScaleP4.pt()*0.001;
      iJet->auxdecor< float >( "pileupScaleEta") = jetPileupScaleP4.eta();
      iJet->auxdecor< float >( "pileupScalePhi") = jetPileupScaleP4.phi();
      iJet->auxdecor< float >( "pileupScaleE") = jetPileupScaleP4.e()*0.001;
      // OriginConstitScale
      if(m_debug) std::cout << "Retrieving JetOriginConstitScaleMomentum" << std::endl;
      xAOD::JetFourMom_t jetOriginConstitScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetOriginConstitScaleMomentum");
      iJet->auxdecor< float >( "originConstitScalepT") = jetOriginConstitScaleP4.pt()*0.001;
      iJet->auxdecor< float >( "originConstitScaleEta") = jetOriginConstitScaleP4.eta();
      iJet->auxdecor< float >( "originConstitScalePhi") = jetOriginConstitScaleP4.phi();
      iJet->auxdecor< float >( "originConstitScaleE") = jetOriginConstitScaleP4.e()*0.001;
      // EtaJESScale
      if(m_debug) std::cout << "Retrieving JetEtaJESScaleMomentum" << std::endl;
      xAOD::JetFourMom_t jetEtaJESScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetEtaJESScaleMomentum");
      iJet->auxdecor< float >( "etaJESScalepT") = jetEtaJESScaleP4.pt()*0.001;
      iJet->auxdecor< float >( "etaJESScaleEta") = jetEtaJESScaleP4.eta();
      iJet->auxdecor< float >( "etaJESScalePhi") = jetEtaJESScaleP4.phi();
      iJet->auxdecor< float >( "etaJESScaleE") = jetEtaJESScaleP4.e()*0.001;
    }

  }
  */

  //--------------
  // Filling Tree
  //--------------

  if(m_debug) std::cout << "Filling Tree" << std::endl;
  if(!m_treeCreated) AddTree("");
  m_myTree->FillEvent( eventInfo, m_event );
  m_myTree->FillTrigger( eventInfo );
  m_myTree->FillJets( RefJets, -1 , "RefJet"); 
  if(m_isMC) m_myTree->FillJets( TruthRefJets, -1 , "TruthRefJet");
  if(m_jetColl=="All"){
    m_myTree->FillJets( RscanJets.at(0), -1 , "2LCJet"); 
    m_myTree->FillJets( RscanJets.at(1), -1 , "3LCJet"); 
    m_myTree->FillJets( RscanJets.at(2), -1 , "5LCJet"); 
    m_myTree->FillJets( RscanJets.at(3), -1 , "6LCJet"); 
    m_myTree->FillJets( RscanJets.at(4), -1 , "7LCJet"); 
    m_myTree->FillJets( RscanJets.at(5), -1 , "8LCJet");
    if(m_isMC && m_saveRscanTruthJets){
      m_myTree->FillJets( TruthRscanJets.at(0), -1 , "Truth2LCJet"); 
      m_myTree->FillJets( TruthRscanJets.at(1), -1 , "Truth3LCJet"); 
      m_myTree->FillJets( TruthRscanJets.at(2), -1 , "Truth5LCJet"); 
      m_myTree->FillJets( TruthRscanJets.at(3), -1 , "Truth6LCJet"); 
      m_myTree->FillJets( TruthRscanJets.at(4), -1 , "Truth7LCJet"); 
      m_myTree->FillJets( TruthRscanJets.at(5), -1 , "Truth8LCJet"); 
    }
  }
  else{
    bool str2LC = false;
    bool str3LC = false;
    bool str5LC = false;
    bool str6LC = false;
    bool str7LC = false;
    bool str8LC = false;
    for(int i=0;i<nRscan;++i){
      if(m_jetColl.Contains("2LC") && !str2LC){
        m_myTree->FillJets( RscanJets.at(i), -1 , "2LCJet"); 
        if(m_isMC && m_saveRscanTruthJets) m_myTree->FillJets( TruthRscanJets.at(i), -1 , "Truth2LCJet"); 
        str2LC = true;
      }
      else if(m_jetColl.Contains("3LC") && !str3LC){
        m_myTree->FillJets( RscanJets.at(i), -1 , "3LCJet"); 
        if(m_isMC && m_saveRscanTruthJets) m_myTree->FillJets( TruthRscanJets.at(i), -1 , "Truth3LCJet"); 
        str3LC = true;
      }
      else if(m_jetColl.Contains("5LC") && !str5LC){
        m_myTree->FillJets( RscanJets.at(i), -1 , "5LCJet"); 
        if(m_isMC && m_saveRscanTruthJets) m_myTree->FillJets( TruthRscanJets.at(i), -1 , "Truth5LCJet"); 
        str5LC = true;
      }
      else if(m_jetColl.Contains("6LC") && !str6LC){
        m_myTree->FillJets( RscanJets.at(i), -1 , "6LCJet"); 
        if(m_isMC && m_saveRscanTruthJets) m_myTree->FillJets( TruthRscanJets.at(i), -1 , "Truth6LCJet"); 
        str6LC = true;
      }
      else if(m_jetColl.Contains("7LC") && !str7LC){
        m_myTree->FillJets( RscanJets.at(i), -1 , "7LCJet"); 
        if(m_isMC && m_saveRscanTruthJets) m_myTree->FillJets( TruthRscanJets.at(i), -1 , "Truth7LCJet"); 
        str7LC = true;
      }
      else if(m_jetColl.Contains("8LC") && !str8LC){
        m_myTree->FillJets( RscanJets.at(i), -1 , "8LCJet"); 
        if(m_isMC && m_saveRscanTruthJets) m_myTree->FillJets( TruthRscanJets.at(i), -1 , "Truth8LCJet"); 
        str8LC = true;
      }
    }
  }
  m_myTree->Fill();

  delete m_IEventInfo;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode RscanInsitu::postExecute() { 

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode RscanInsitu::finalize() { 

  Info("RscanInsitu()", "Calling finalize \n");

  std::cout << "Done!" << std::endl;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode RscanInsitu :: histFinalize ()
{

  Info("RscanInsitu()", "Calling histFinalize \n");

  return EL::StatusCode::SUCCESS;
}




//----------------------
//  Personal Functions
//----------------------

void RscanInsitu::setConfig(std::string configName){
  m_configName = configName;
}

void RscanInsitu::setJetColl(std::string jetColl){
  m_jetColl = jetColl;
}

