#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "xAODRscanInsitu/MiniTree.h"

MiniTree :: MiniTree(xAOD::TEvent * event, TTree* tree, TFile* file) :
  HelpTreeBase(event, tree, file, 1e3)
{
  Info("MiniTree", "Creating output TTree");
}

MiniTree :: ~MiniTree()
{
}
//////////////////// Connect Defined variables to branches here /////////////////////////////
void MiniTree::AddEventUser(const std::string detailStr)
{
}

void MiniTree::AddJetsUser(const std::string detailStr, const std::string jetName)
{

  m_jetScales[jetName] = new jetScales();
  
  jetScales* thisJet = m_jetScales[jetName];
  
  m_tree->Branch((jetName+"_constScaleEta").c_str(), &thisJet->m_jet_constScaleEta);
  m_tree->Branch((jetName+"_pileupScaleEta").c_str(), &thisJet->m_jet_pileupScaleEta);
  m_tree->Branch((jetName+"_originConstitScaleEta").c_str(), &thisJet->m_jet_originConstitScaleEta);
  m_tree->Branch((jetName+"_etaJESScaleEta").c_str(), &thisJet->m_jet_etaJESScaleEta);
  m_tree->Branch((jetName+"_gscScaleEta").c_str(), &thisJet->m_jet_gscScaleEta);
  m_tree->Branch((jetName+"_insituScaleEta").c_str(), &thisJet->m_jet_insituScaleEta);

}

//////////////////// Clear any defined vectors here ////////////////////////////
void MiniTree::ClearEventUser() {
}

void MiniTree::ClearJetsUser(const std::string jetName) {
  
  jetScales* thisJet = m_jetScales[jetName];

  thisJet->m_jet_constScaleEta.clear();
  thisJet->m_jet_pileupScaleEta.clear();
  thisJet->m_jet_originConstitScaleEta.clear();
  thisJet->m_jet_etaJESScaleEta.clear();
  thisJet->m_jet_gscScaleEta.clear();
  thisJet->m_jet_insituScaleEta.clear();

}

/////////////////// Assign values to defined event variables here ////////////////////////
void MiniTree::FillEventUser( const xAOD::EventInfo* eventInfo ) {


}

/////////////////// Assign values to defined jet variables here //////////////////
void MiniTree::FillJetsUser( const xAOD::Jet* jet, const std::string jetName ) {

  jetScales* thisJet = m_jetScales[jetName];

  // ConstitScale
  if( jet->isAvailable< float >( "constScaleEta" ) ) {
    thisJet->m_jet_constScaleEta.push_back( jet->auxdata< float >("constScaleEta") );
  } else {
    thisJet->m_jet_constScaleEta.push_back( -999 );
  }

  // PileupScale
  if( jet->isAvailable< float >( "pileupScaleEta" ) ) {
    thisJet->m_jet_pileupScaleEta.push_back( jet->auxdata< float >("pileupScaleEta") );
  } else {
    thisJet->m_jet_pileupScaleEta.push_back( -999 );
  }

  // OriginConstitScale
  if( jet->isAvailable< float >( "originConstitScaleEta" ) ) {
    thisJet->m_jet_originConstitScaleEta.push_back( jet->auxdata< float >("originConstitScaleEta") );
  } else {
    thisJet->m_jet_originConstitScaleEta.push_back( -999 );
  }

  // EtaJESScale
  if( jet->isAvailable< float >( "etaJESScaleEta" ) ) {
    thisJet->m_jet_etaJESScaleEta.push_back( jet->auxdata< float >("etaJESScaleEta") );
  } else {
    thisJet->m_jet_etaJESScaleEta.push_back( -999 );
  }

  // GSCScale
  if( jet->isAvailable< float >( "gscScaleEta" ) ) {
    thisJet->m_jet_gscScaleEta.push_back( jet->auxdata< float >("gscScaleEta") );
  } else {
    thisJet->m_jet_gscScaleEta.push_back( -999 );
  }

  // InsituScale
  if( jet->isAvailable< float >( "insituScaleEta" ) ) {
    thisJet->m_jet_insituScaleEta.push_back( jet->auxdata< float >("insituScaleEta") );
  } else {
    thisJet->m_jet_insituScaleEta.push_back( -999 );
  }

}

void MiniTree::AddTruthJets()
{
/*
  if(m_debug) Info("AddTruthJets()", "Adding kinematics jet variables");

  m_tree->Branch("ntruthjets",    &m_ntruthjet,"ntruthjets/I");
  m_tree->Branch("truthjet_E",   &m_truthjet_E);
  m_tree->Branch("truthjet_pt",  &m_truthjet_pt);
  m_tree->Branch("truthjet_phi", &m_truthjet_phi);
  m_tree->Branch("truthjet_eta", &m_truthjet_eta);
  m_tree->Branch("truthjet_rapidity", &m_truthjet_rapidity);
*/
}

void MiniTree::ClearTruthJets() {
/*
  m_ntruthjet = 0;
  m_truthjet_pt.clear();
  m_truthjet_eta.clear();
  m_truthjet_phi.clear();
  m_truthjet_E.clear();
  m_truthjet_rapidity.clear();
  */

}

void MiniTree::FillTruthJets( const xAOD::JetContainer* jets) {
/*
  this->ClearTruthJets();

  for( auto jet_itr : *jets ) {

    m_truthjet_pt.push_back ( jet_itr->pt() / m_units );
    m_truthjet_eta.push_back( jet_itr->eta() );
    m_truthjet_phi.push_back( jet_itr->phi() );
    m_truthjet_E.push_back  ( jet_itr->e() / m_units );
    m_truthjet_rapidity.push_back( jet_itr->rapidity() );

    m_ntruthjet++;

  } // loop over jets
  */
}


