#ifndef xAODRscanInsitu_MiniTree_H
#define xAODRscanInsitu_MiniTree_H

#include "xAODAnaHelpers/HelpTreeBase.h"
#include "TTree.h"

class MiniTree : public HelpTreeBase
{

  private:

    struct jetScales{

      std::vector<float> m_jet_constScaleEta;
      std::vector<float> m_jet_pileupScaleEta;
      std::vector<float> m_jet_originConstitScaleEta;
      std::vector<float> m_jet_etaJESScaleEta;
      std::vector<float> m_jet_gscScaleEta;
      std::vector<float> m_jet_insituScaleEta;

      jetScales(){ }

    };

    std::map<std::string, jetScales*> m_jetScales;

    /*
    int m_ntruthjet;
    std::vector<float> m_truthjet_pt;
    std::vector<float> m_truthjet_eta;
    std::vector<float> m_truthjet_phi;
    std::vector<float> m_truthjet_E;
    std::vector<float> m_truthjet_rapidity;
    */

  public:

    MiniTree(xAOD::TEvent * event, TTree* tree, TFile* file);
    ~MiniTree();

    void AddEventUser( const std::string detailStr = "" );
    void AddJetsUser( const std::string detailStr = "" , const std::string jetName = "jet");
    void FillEventUser( const xAOD::EventInfo* eventInfo );
    void FillJetsUser( const xAOD::Jet* jet, const std::string jetName = "jet" );
    void ClearEventUser();
    void ClearJetsUser(const std::string jetName = "jet");

    void AddTruthJets();
    void ClearTruthJets();
    void FillTruthJets( const xAOD::JetContainer* jets);

};
#endif
