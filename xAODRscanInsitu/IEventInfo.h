#ifndef xAODRscanInsitu_IEventInfo_H
#define xAODRscanInsitu_IEventInfo_H

#include <vector>
#include <string>
#include <iostream>

//_________________________________________________________________________________________________
// ROOT specific header files
#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <TStopwatch.h>
#include <TRandom.h>
#include <TVector2.h>
#include <TLeaf.h>
#include <TString.h>
#include <TMath.h>
#include <TROOT.h>
#include <TList.h>

class IEventInfo {

  private:

    /////////////////////////////////////////////////////////////
    // From D3PD.h
    UInt_t                RunNumber;//!
    UInt_t                EventNumber;//!
    UInt_t                larError;//!
    UInt_t                lbn;//!
    UInt_t                bcid;//!
    UInt_t                bunch_configID;//!
    Float_t               averageIntPerXing;//!

    double mcevt_weight;//! XQ vector de vectores???
    //std::vector<std::vector<double> > *mcevt_weight;//!
    
  public:
 
    IEventInfo(); //Constructor

    // Functions
    void SetRunNumber(UInt_t runnumber){RunNumber = runnumber;}
    void SetEventNumber(UInt_t eventnumber){EventNumber = eventnumber;}
    void SetlarError(UInt_t larerror){larError = larerror;}
    void Setlbn(UInt_t lbn_aux){lbn = lbn_aux;}
    void Setbcid(UInt_t bcid_aux){bcid = bcid_aux;}
    void Setbunch_configID(UInt_t bunch_configid){bunch_configID = bunch_configid;}
    void SetaverageIntPerXing(UInt_t averageintperxing){averageIntPerXing = averageintperxing;}
    void Setmcevt_weight(double mcevtweight){mcevt_weight = mcevtweight;}

    UInt_t GetRunNumber(){return RunNumber;}
    UInt_t GetEventNumber(){return EventNumber;}
    UInt_t GetlarError(){return larError;}
    UInt_t Getlbn(){return lbn;}
    UInt_t Getbcid(){return bcid;}
    UInt_t Getbunch_configID(){return bunch_configID;}
    UInt_t GetaverageIntPerXing(){return averageIntPerXing;}
    double Getmcevt_weight(){return mcevt_weight;}
};

#endif

