#ifndef xAODRscanInsitu_RscanInsitu_H
#define xAODRscanInsitu_RscanInsitu_H

// inlude the parent class header
#include <EventLoop/Algorithm.h>
#include "xAODRscanInsitu/IEventInfo.h"
#include "xAODRscanInsitu/MiniTree.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

// ROOT specific header files
#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <TH3D.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <TStopwatch.h>
#include <TRandom.h>
#include <TVector2.h>
#include <TLeaf.h>
#include <TString.h>
#include <TMath.h>
#include <TROOT.h>
#include <TList.h>
#include "TEnv.h"

class RscanInsitu : public EL::Algorithm
{

  protected:

    xAOD::TEvent*                m_event; //!
    xAOD::TStore*                m_store; //!

    int m_eventCounter; //!
    
    std::string m_configName;
    TString m_jetColl;

  private:  

    //MiniTree* m_miniTree; //!
    MiniTree* m_myTree; //!

    bool m_debug; //!
    bool m_isMC; //!
    bool m_treeCreated; //!

    std::string m_treeStream;

    bool m_saveRscanTruthJets; //! rscan truth jets add to tree
    std::string m_eventDetailStr;     //! event info add to tree
    std::string m_trigDetailStr;      //! trigger info add to tree
    std::string m_jetDetailStr;       //! jet info add to tree
    std::string m_jetDetailStrSyst;   //! jetsyst info add to tree
    std::string m_inputAlgo;          //! input algo for when running systs

    //-----------
    // HISTOGRAMS
    //-----------

    //TH1D* h_number_skipped_events;//!
    //TH1D* h_number_skipped_events_weighted;//!
    
    IEventInfo *m_IEventInfo;//!

    double GeV = 0.001;


  public:

    // this is a standard constructor
    RscanInsitu ();

    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& job);
    virtual EL::StatusCode fileExecute ();
    virtual EL::StatusCode histInitialize ();
    virtual EL::StatusCode changeInput (bool firstFile);
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode postExecute ();
    virtual EL::StatusCode finalize ();
    virtual EL::StatusCode histFinalize ();

    void setConfig(std::string configName);
    void setJetColl(std::string jetColl);

    void AddTree(std::string);

  // this is needed to distribute the algorithm to the workers
  ClassDef(RscanInsitu, 1);
};

#endif
