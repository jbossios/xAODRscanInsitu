#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/GridDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/ScanDir.h"
#include <TSystem.h>

#include "xAODRscanInsitu/RscanInsitu.h"

#include "xAODAnaHelpers/BasicEventSelection.h"
#include "xAODAnaHelpers/JetCalibrator.h"
#include "xAODAnaHelpers/JetSelector.h"

#include <sstream>

#include <TStopwatch.h>


int main( int argc, char* argv[] ) {

  //---------------------------------------------
  // Declaring input variables with default values
  //---------------------------------------------
  std::string submitDir = "submitDir"; //Output file name
  std::string systName = ""; // Only Nominal
  std::string sample; // Sample directory (Locally), dq2 sample (grid)
  std::string jetColl; // set of rscan jet collections to use
  bool grid = false; // Default: false
  bool mc = true; // Default: true
  bool debug = false; // Default: false
  bool saveRscanTruth = true;
  bool rscanInsitu = false;
  float systVal = 1;

  // Grid only
  std::string USER_output_path; // user.jbossios.something

  //---------------------------
  // Decoding the user settings
  //---------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
        v.push_back(item);
    }

    // Take the submit directory from the input:
    if ( opt.find("--submitDir=")   != std::string::npos) submitDir = v[1];

    // Output Dataset
    if ( opt.find("--output=")     != std::string::npos) USER_output_path= v[1];

    // Input Dataset
    if ( opt.find("--sample=")   != std::string::npos) sample = v[1];
    
    if ( opt.find("--rscanInsitu=")   != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) rscanInsitu = true;
       if (v[1].find("FALSE") != std::string::npos) rscanInsitu = false;
    }

    if ( opt.find("--mc=")   != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) mc = true;
       if (v[1].find("FALSE") != std::string::npos) mc = false;
    }

    if ( opt.find("--saveRscanTruth=")   != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) saveRscanTruth = true;
       if (v[1].find("FALSE") != std::string::npos) saveRscanTruth = false;
    }

    if ( opt.find("--jetColl=")   != std::string::npos) jetColl = v[1];

    if ( opt.find("--Debug=")   != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) debug = true;
       if (v[1].find("FALSE") != std::string::npos) debug = false;
    }

    if ( opt.find("--grid=")   != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) grid = true;
       if (v[1].find("FALSE") != std::string::npos) grid = false;
    }

  }//End: Loop over input options

  TString JetCollection = jetColl;
 
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  if(grid) SH::scanDQ2 (sh, sample);
  else{SH::ScanDir().scan(sh,sample);}

  // Set the name of the input TTree. It's always "CollectionTree" for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );
  //sh.setMetaString( "nc_excludeSite", "ANALY_RAL_SL6");
  //sh.setMetaString( "nc_excludeSite", "CA ANALY_SFU");
  //sh.setMetaString( "nc_excludeSite", "ANALY_SLAC");
  //sh.setMetaString( "nc_excludeSite", "ANALY_SFU");
  //sh.setMetaString( "nc_excludeSite", "ANALY_mcGILL");

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );

  if(grid){
    // To automatically delete submitDir
    job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);

    // For Trigger
    job.options()->setString( EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_branch );
  }

  //------------------------------
  // Setting the config file names
  //------------------------------

  // RscanInsitu
  std::string analysisConfig    = "$ROOTCOREBIN/data/xAODRscanInsitu/smanalysis";
  if(debug)   analysisConfig   += "_Debug";
  else if(!mc) analysisConfig   += "_Data";
  else if(mc && !saveRscanTruth) analysisConfig += "_wo_RscanTruth";
  analysisConfig               += ".config";
  // BasicEventSelectoin
  std::string baseEventConfig    = "$ROOTCOREBIN/data/xAODRscanInsitu/baseEvent";
  if(!mc)     baseEventConfig   += "_Data";
  baseEventConfig               += ".config";
  // JetCalibrator
  //4EM
  std::string jetCalibratorConfig_4EM = "$ROOTCOREBIN/data/xAODRscanInsitu/jetCalib_AntiKt4EMTopoCalib";
  if(!mc)     jetCalibratorConfig_4EM += "_Data";
  jetCalibratorConfig_4EM             += ".config";
  //2LC
  std::string jetCalibratorConfig_2LC = "$ROOTCOREBIN/data/xAODRscanInsitu/jetCalib_AntiKt2LCTopoCalib";
  if(!mc)     jetCalibratorConfig_2LC += "_Data";
  if(!mc && rscanInsitu) jetCalibratorConfig_2LC += "_Insitu";
  jetCalibratorConfig_2LC             += ".config";
  //3LC
  std::string jetCalibratorConfig_3LC = "$ROOTCOREBIN/data/xAODRscanInsitu/jetCalib_AntiKt3LCTopoCalib";
  if(!mc)     jetCalibratorConfig_3LC += "_Data";
  if(!mc && rscanInsitu) jetCalibratorConfig_3LC += "_Insitu";
  jetCalibratorConfig_3LC             += ".config";
  //5LC
  std::string jetCalibratorConfig_5LC = "$ROOTCOREBIN/data/xAODRscanInsitu/jetCalib_AntiKt5LCTopoCalib";
  if(!mc)     jetCalibratorConfig_5LC += "_Data";
  if(!mc && rscanInsitu) jetCalibratorConfig_5LC += "_Insitu";
  jetCalibratorConfig_5LC             += ".config";
  //6LC
  std::string jetCalibratorConfig_6LC = "$ROOTCOREBIN/data/xAODRscanInsitu/jetCalib_AntiKt6LCTopoCalib";
  if(!mc)     jetCalibratorConfig_6LC += "_Data";
  if(!mc && rscanInsitu) jetCalibratorConfig_6LC += "_Insitu";
  jetCalibratorConfig_6LC             += ".config";
  //7LC
  std::string jetCalibratorConfig_7LC = "$ROOTCOREBIN/data/xAODRscanInsitu/jetCalib_AntiKt7LCTopoCalib";
  if(!mc)     jetCalibratorConfig_7LC += "_Data";
  if(!mc && rscanInsitu) jetCalibratorConfig_7LC += "_Insitu";
  jetCalibratorConfig_7LC             += ".config";
  //8LC
  std::string jetCalibratorConfig_8LC = "$ROOTCOREBIN/data/xAODRscanInsitu/jetCalib_AntiKt8LCTopoCalib";
  if(!mc)     jetCalibratorConfig_8LC += "_Data";
  if(!mc && rscanInsitu) jetCalibratorConfig_8LC += "_Insitu";
  jetCalibratorConfig_8LC             += ".config";
  // JetSelector
  std::string jetSelectorConfig_4EM  = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_4EM.config";
  std::string jetSelectorConfig_2LC  = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_2LC.config";
  std::string jetSelectorConfig_3LC  = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_3LC.config";
  std::string jetSelectorConfig_5LC  = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_5LC.config";
  std::string jetSelectorConfig_6LC  = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_6LC.config";
  std::string jetSelectorConfig_7LC  = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_7LC.config";
  std::string jetSelectorConfig_8LC  = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_8LC.config";
  std::string jetSelectorConfig_T4   = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_Truth_4.config";
  std::string jetSelectorConfig_T2   = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_Truth_2.config";
  std::string jetSelectorConfig_T3   = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_Truth_3.config";
  std::string jetSelectorConfig_T5   = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_Truth_5.config";
  std::string jetSelectorConfig_T6   = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_Truth_6.config";
  std::string jetSelectorConfig_T7   = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_Truth_7.config";
  std::string jetSelectorConfig_T8   = "$ROOTCOREBIN/data/xAODRscanInsitu/jetSelect_signal_Truth_8.config";

  // Showing the config files used
  std::cout << "Config files:" << std::endl;
  std::cout << "xAODRscanInsituConfig = " << analysisConfig.c_str() << std::endl;
  std::cout << "BasicEventSelectionConfig = " << baseEventConfig.c_str() << std::endl;
  std::cout << "jetCalibratorConfig_4EM = " << jetCalibratorConfig_4EM.c_str() << std::endl;
  std::cout << "jetSelectortorConfig_4EM = " << jetSelectorConfig_4EM.c_str() << "\n" << std::endl;
  std::cout << "jetCalibratorConfig_2LC = " << jetCalibratorConfig_2LC.c_str() << std::endl;
  std::cout << "jetSelectortorConfig_2LC = " << jetSelectorConfig_2LC.c_str() << "\n" << std::endl;
  std::cout << "jetCalibratorConfig_3LC = " << jetCalibratorConfig_3LC.c_str() << std::endl;
  std::cout << "jetSelectortorConfig_3LC = " << jetSelectorConfig_3LC.c_str() << "\n" << std::endl;
  std::cout << "jetCalibratorConfig_5LC = " << jetCalibratorConfig_5LC.c_str() << std::endl;
  std::cout << "jetSelectortorConfig_5LC = " << jetSelectorConfig_5LC.c_str() << "\n" << std::endl;
  std::cout << "jetCalibratorConfig_6LC = " << jetCalibratorConfig_6LC.c_str() << std::endl;
  std::cout << "jetSelectortorConfig_6LC = " << jetSelectorConfig_6LC.c_str() << "\n" << std::endl;
  std::cout << "jetCalibratorConfig_7LC = " << jetCalibratorConfig_7LC.c_str() << std::endl;
  std::cout << "jetSelectortorConfig_7LC = " << jetSelectorConfig_7LC.c_str() << "\n" << std::endl;
  std::cout << "jetCalibratorConfig_8LC = " << jetCalibratorConfig_8LC.c_str() << std::endl;
  std::cout << "jetSelectortorConfig_8LC = " << jetSelectorConfig_8LC.c_str() << "\n" << std::endl;
  std::cout << "jetSelectortorConfig_T4 = " << jetSelectorConfig_T4.c_str() << "\n" << std::endl;
  std::cout << "jetSelectortorConfig_T2 = " << jetSelectorConfig_T2.c_str() << "\n" << std::endl;
  std::cout << "jetSelectortorConfig_T3 = " << jetSelectorConfig_T3.c_str() << "\n" << std::endl;
  std::cout << "jetSelectortorConfig_T5 = " << jetSelectorConfig_T5.c_str() << "\n" << std::endl;
  std::cout << "jetSelectortorConfig_T6 = " << jetSelectorConfig_T6.c_str() << "\n" << std::endl;
  std::cout << "jetSelectortorConfig_T7 = " << jetSelectorConfig_T7.c_str() << "\n" << std::endl;
  std::cout << "jetSelectortorConfig_T8 = " << jetSelectorConfig_T8.c_str() << "\n" << std::endl;

  //----------------------------
  // Initializing the Algorithms
  //----------------------------

  // BasicEventSelection : GRL, event cleaning, NPV
  BasicEventSelection* baseEventSel = new BasicEventSelection();
  baseEventSel->setName("baseEventSel")->setConfig(baseEventConfig.c_str());

  // JetCalibrator
  //4EM
  JetCalibrator* jetCalib_4EM = new JetCalibrator();
  jetCalib_4EM->setName("jetCalib_AntiKt4EMTopo")->setConfig(jetCalibratorConfig_4EM.c_str())->setSyst( systName, systVal );
  //2LC
  JetCalibrator* jetCalib_2LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("2LC")){
    jetCalib_2LC = new JetCalibrator();
    jetCalib_2LC->setName("jetCalib_AntiKt2LCTopo")->setConfig(jetCalibratorConfig_2LC.c_str())->setSyst( systName, systVal );
  }
  //3LC
  JetCalibrator* jetCalib_3LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("3LC")){
    jetCalib_3LC = new JetCalibrator();
    jetCalib_3LC->setName("jetCalib_AntiKt3LCTopo")->setConfig(jetCalibratorConfig_3LC.c_str())->setSyst( systName, systVal );
  }
  //5LC
  JetCalibrator* jetCalib_5LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("5LC")){
    jetCalib_5LC = new JetCalibrator();
    jetCalib_5LC->setName("jetCalib_AntiKt5LCTopo")->setConfig(jetCalibratorConfig_5LC.c_str())->setSyst( systName, systVal );
  }
  //6LC
  JetCalibrator* jetCalib_6LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("6LC")){
    jetCalib_6LC = new JetCalibrator();
    jetCalib_6LC->setName("jetCalib_AntiKt6LCTopo")->setConfig(jetCalibratorConfig_6LC.c_str())->setSyst( systName, systVal );
  }
  //7LC
  JetCalibrator* jetCalib_7LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("7LC")){
    jetCalib_7LC = new JetCalibrator();
    jetCalib_7LC->setName("jetCalib_AntiKt7LCTopo")->setConfig(jetCalibratorConfig_7LC.c_str())->setSyst( systName, systVal );
  }
  //8LC
  JetCalibrator* jetCalib_8LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("8LC")){
    jetCalib_8LC = new JetCalibrator();
    jetCalib_8LC->setName("jetCalib_AntiKt8LCTopo")->setConfig(jetCalibratorConfig_8LC.c_str())->setSyst( systName, systVal );
  }

  // JetSelector
  //4EM
  JetSelector* jetSelect_signal_4EM = new JetSelector();
  jetSelect_signal_4EM->setName("jetSelect_signal_4EM")->setConfig(jetSelectorConfig_4EM.c_str());
  //2LC
  JetSelector* jetSelect_signal_2LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("2LC")){
    jetSelect_signal_2LC = new JetSelector();
    jetSelect_signal_2LC->setName("jetSelect_signal_2LC")->setConfig(jetSelectorConfig_2LC.c_str());
  }
  //3LC
  JetSelector* jetSelect_signal_3LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("3LC")){
    jetSelect_signal_3LC = new JetSelector();
    jetSelect_signal_3LC->setName("jetSelect_signal_3LC")->setConfig(jetSelectorConfig_3LC.c_str());
  }
  //5LC
  JetSelector* jetSelect_signal_5LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("5LC")){
    jetSelect_signal_5LC = new JetSelector();
    jetSelect_signal_5LC->setName("jetSelect_signal_5LC")->setConfig(jetSelectorConfig_5LC.c_str());
  }
  //6LC
  JetSelector* jetSelect_signal_6LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("6LC")){
    jetSelect_signal_6LC = new JetSelector();
    jetSelect_signal_6LC->setName("jetSelect_signal_6LC")->setConfig(jetSelectorConfig_6LC.c_str());
  }
  //7LC
  JetSelector* jetSelect_signal_7LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("7LC")){
    jetSelect_signal_7LC = new JetSelector();
    jetSelect_signal_7LC->setName("jetSelect_signal_7LC")->setConfig(jetSelectorConfig_7LC.c_str());
  }
  //8LC
  JetSelector* jetSelect_signal_8LC = 0;
  if(JetCollection=="All" || JetCollection.Contains("8LC")){
    jetSelect_signal_8LC = new JetSelector();
    jetSelect_signal_8LC->setName("jetSelect_signal_8LC")->setConfig(jetSelectorConfig_8LC.c_str()); 
  }
  // Truth 4EM
  JetSelector* jetSelect_signal_T4 = new JetSelector();
  jetSelect_signal_T4->setName("jetSelect_signal_T4")->setConfig(jetSelectorConfig_T4.c_str());
  // Truth Rscan
  JetSelector* jetSelect_signal_T2 = 0;
  JetSelector* jetSelect_signal_T3 = 0;
  JetSelector* jetSelect_signal_T5 = 0;
  JetSelector* jetSelect_signal_T6 = 0;
  JetSelector* jetSelect_signal_T7 = 0;
  JetSelector* jetSelect_signal_T8 = 0;
  if(saveRscanTruth){
    if(JetCollection=="All" || JetCollection.Contains("2LC")){
      jetSelect_signal_T2 = new JetSelector();
      jetSelect_signal_T2->setName("jetSelect_signal_T2")->setConfig(jetSelectorConfig_T2.c_str());
    }
    //Truth3
     if(JetCollection=="All" || JetCollection.Contains("3LC")){
     jetSelect_signal_T3 = new JetSelector();
      jetSelect_signal_T3->setName("jetSelect_signal_T3")->setConfig(jetSelectorConfig_T3.c_str());
    }
    //Truth5
    if(JetCollection=="All" || JetCollection.Contains("5LC")){
      jetSelect_signal_T5 = new JetSelector();
      jetSelect_signal_T5->setName("jetSelect_signal_T5")->setConfig(jetSelectorConfig_T5.c_str());
    }
    //Truth6
    if(JetCollection=="All" || JetCollection.Contains("6LC")){
      jetSelect_signal_T6 = new JetSelector();
      jetSelect_signal_T6->setName("jetSelect_signal_T6")->setConfig(jetSelectorConfig_T6.c_str());
    }
    //Truth7
    if(JetCollection=="All" || JetCollection.Contains("7LC")){
      jetSelect_signal_T7 = new JetSelector();
      jetSelect_signal_T7->setName("jetSelect_signal_T7")->setConfig(jetSelectorConfig_T7.c_str());
    }
    //Truth8
    if(JetCollection=="All" || JetCollection.Contains("8LC")){
      jetSelect_signal_T8 = new JetSelector();
      jetSelect_signal_T8->setName("jetSelect_signal_T8")->setConfig(jetSelectorConfig_T8.c_str());
    }
  }

  // RscanInsitu
  RscanInsitu* alg = new RscanInsitu();
  alg->setConfig( analysisConfig.c_str() );
  alg->setJetColl( JetCollection.Data() );

  //-----------------------------
  // Adding algorithms to the job
  //-----------------------------
  job.algsAdd( baseEventSel );
  job.algsAdd( jetCalib_4EM );
  if(JetCollection=="All" || JetCollection.Contains("2LC")) job.algsAdd( jetCalib_2LC );
  if(JetCollection=="All" || JetCollection.Contains("3LC")) job.algsAdd( jetCalib_3LC );
  if(JetCollection=="All" || JetCollection.Contains("5LC")) job.algsAdd( jetCalib_5LC );
  if(JetCollection=="All" || JetCollection.Contains("6LC")) job.algsAdd( jetCalib_6LC );
  if(JetCollection=="All" || JetCollection.Contains("7LC")) job.algsAdd( jetCalib_7LC );
  if(JetCollection=="All" || JetCollection.Contains("8LC")) job.algsAdd( jetCalib_8LC );
  job.algsAdd( jetSelect_signal_4EM );
  if(JetCollection=="All" || JetCollection.Contains("2LC")) job.algsAdd( jetSelect_signal_2LC );
  if(JetCollection=="All" || JetCollection.Contains("3LC")) job.algsAdd( jetSelect_signal_3LC );
  if(JetCollection=="All" || JetCollection.Contains("5LC")) job.algsAdd( jetSelect_signal_5LC );
  if(JetCollection=="All" || JetCollection.Contains("6LC")) job.algsAdd( jetSelect_signal_6LC );
  if(JetCollection=="All" || JetCollection.Contains("7LC")) job.algsAdd( jetSelect_signal_7LC );
  if(JetCollection=="All" || JetCollection.Contains("8LC")) job.algsAdd( jetSelect_signal_8LC );
  if(mc){
    job.algsAdd( jetSelect_signal_T4 );
    if(saveRscanTruth){
      if(JetCollection=="All" || JetCollection.Contains("2LC")) job.algsAdd( jetSelect_signal_T2 );
      if(JetCollection=="All" || JetCollection.Contains("3LC")) job.algsAdd( jetSelect_signal_T3 );
      if(JetCollection=="All" || JetCollection.Contains("5LC")) job.algsAdd( jetSelect_signal_T5 );
      if(JetCollection=="All" || JetCollection.Contains("6LC")) job.algsAdd( jetSelect_signal_T6 );
      if(JetCollection=="All" || JetCollection.Contains("7LC")) job.algsAdd( jetSelect_signal_T7 );
      if(JetCollection=="All" || JetCollection.Contains("8LC")) job.algsAdd( jetSelect_signal_T8 );
    }
  }
  job.algsAdd( alg );

  if(!grid){
    // Run the job using the local/direct driver:
    EL::DirectDriver driver;
    driver.submit( job, submitDir );

    std::cout << "###########################################" << std::endl;
    std::cout << "Finished" << std::endl;
    std::cout << "###########################################" << std::endl;
  }
  else{

    EL::PrunDriver driver;
    //EL::GridDriver driver;

    driver.options()->setString("nc_outputSampleName", USER_output_path);
    driver.options()->setDouble("nc_nFilesPerJob", 1);
    driver.options()->setString(EL::Job::optGridExcludedSite, "ANALY_BNL_LONG");
    driver.options()->setString(EL::Job::optGridExcludedSite, "ANALY_BNL_SHORT");
    //driver.options()->setString("nc_cmtConfig", "x86_64-slc6-gcc48-opt");
  
    // From DijetResonance
    driver.options()->setDouble(EL::Job::optGridNFilesPerJob, 2);
    //driver.options()->setDouble(EL::Job::optGridMemory,10240); // 10 GB

    // Run the job using prun driver:
    driver.submitOnly( job, submitDir );
 
    std::cout << "###########################################" << std::endl;
    std::cout << "Submitted" << std::endl;
    std::cout << "###########################################" << std::endl;
  }

  return 0;
}



