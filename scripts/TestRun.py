#!/usr/bin/python

import os, sys

param=sys.argv

DEBUG = False
MC    = False
#jetColl = "6LC"
#jetColl = "All"
#jetColl = "2LC6LC"
jetColl = "2LC6LC8LC"
saveRscanTruthJets = False
RscanInsitu = False # If False the Insitu is not applied to Rscan jets
# Test sample
sample = "/afs/cern.ch/user/j/USER/work/public/Sample"

##################################################################################
#
##################################################################################


command = "nohup RunRscanAnalysis "

if DEBUG:
    command += "--Debug=TRUE "

if not MC:
    command += "--mc=FALSE "

# Outputs
command += "--submitDir=OutputDir"

command += " --grid=FALSE"

if not saveRscanTruthJets:
    command += " --saveRscanTruth=FALSE"

command += " --jetColl="
command += jetColl

if RscanInsitu:
    command += " --rscanInsitu=TRUE"

# Sample
command += " --sample=" + sample + " > "

# Logfile's Path
command += "logfile" + ".log 2> "

# Errors File's Path
command += "errors" + ".err &"

print command
os.system(command)

