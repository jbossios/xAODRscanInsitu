#!/usr/bin/python

import os, sys

param=sys.argv

# Check
# Date
date = "XX_YY_ZZZZ_EXAMPLE"

# Check
user = "USER"

# Check (rscan jet collections)
#JetCollection = "6LC" 
#JetCollection = "2LC6LC8LC" 
JetCollection = "All"

# Check
#RscanInsitu = True
RscanInsitu = False

# Check
MC  = True

# Check
SaveRscanTruth = False
#SaveRscanTruth = True

# Check
version = "v_1"

JETM9_25ns_MC_Pythia       = False # MC15a
JETM9_25ns_MC_Powheg       = False # MC15a
JETM9_25ns_Data      	   = False
JETM9_25ns_MC_Herwig 	   = False  # MC15a
JETM9_25ns_MC_Pythia_MC15b = False
JETM9_25ns_MC_Powheg_MC15b = False
JETM9_25ns_MC_Sherpa_MC15a = True

list=[
    "JZ1",
    "JZ2",
    "JZ3",
    "JZ4",
    "JZ5",
    "JZ6", # Sherpa up to JZ6 only
#    "JZ7",
#    "JZ8",
#    "JZ9", # not available for MC15a Pythia
]


JETM9Data=[
   "data15_13TeV:data15_13TeV.00276262.physics_Main.merge.DAOD_JETM9.f620_m1480_p2425", #D3
   "data15_13TeV:data15_13TeV.00276329.physics_Main.merge.DAOD_JETM9.f620_m1480_p2425", #D3
   "data15_13TeV:data15_13TeV.00276336.physics_Main.merge.DAOD_JETM9.f620_m1480_p2425", #D3
   "data15_13TeV:data15_13TeV.00276416.physics_Main.merge.DAOD_JETM9.f620_m1480_p2425", #D4
   "data15_13TeV:data15_13TeV.00276511.physics_Main.merge.DAOD_JETM9.f620_m1480_p2425", #D4
   "data15_13TeV:data15_13TeV.00276689.physics_Main.merge.DAOD_JETM9.f623_m1480_p2425", #D4
   "data15_13TeV:data15_13TeV.00276778.physics_Main.merge.DAOD_JETM9.f620_m1480_p2425", #D6
   "data15_13TeV:data15_13TeV.00276790.physics_Main.merge.DAOD_JETM9.f620_m1480_p2425", #D6
   "data15_13TeV:data15_13TeV.00276952.physics_Main.merge.DAOD_JETM9.f620_m1480_p2425", #D6
   "data15_13TeV:data15_13TeV.00276954.physics_Main.merge.DAOD_JETM9.f620_m1480_p2425", #D6
   "data15_13TeV:data15_13TeV.00278880.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E2
   "data15_13TeV:data15_13TeV.00278912.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E2
   "data15_13TeV:data15_13TeV.00278968.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E3
   "data15_13TeV:data15_13TeV.00279169.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E3
   "data15_13TeV:data15_13TeV.00279259.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E3
   "data15_13TeV:data15_13TeV.00279279.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E3
   "data15_13TeV:data15_13TeV.00279284.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E3
   "data15_13TeV:data15_13TeV.00279345.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E4
   "data15_13TeV:data15_13TeV.00279515.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E4
   "data15_13TeV:data15_13TeV.00279598.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E4
   "data15_13TeV:data15_13TeV.00279685.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E4
   "data15_13TeV:data15_13TeV.00279764.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E5
   "data15_13TeV:data15_13TeV.00279813.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E5
   "data15_13TeV:data15_13TeV.00279867.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E5
   "data15_13TeV:data15_13TeV.00279928.physics_Main.merge.DAOD_JETM9.f628_m1497_p2425", #E5
   "data15_13TeV:data15_13TeV.00279932.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #F1
   "data15_13TeV:data15_13TeV.00279984.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #F1
   "data15_13TeV:data15_13TeV.00280231.physics_Main.merge.DAOD_JETM9.f630_m1504_p2425", #G1
   "data15_13TeV:data15_13TeV.00280319.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G1
   "data15_13TeV:data15_13TeV.00280368.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G1
   "data15_13TeV:data15_13TeV.00280423.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G1
   "data15_13TeV:data15_13TeV.00280464.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G1
   "data15_13TeV:data15_13TeV.00280500.physics_Main.merge.DAOD_JETM9.f631_m1504_p2425", #G2
   "data15_13TeV:data15_13TeV.00280520.physics_Main.merge.DAOD_JETM9.f632_m1504_p2425", #G2
   "data15_13TeV:data15_13TeV.00280614.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G2
   "data15_13TeV:data15_13TeV.00280673.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G2
   "data15_13TeV:data15_13TeV.00280753.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G2
   "data15_13TeV:data15_13TeV.00280853.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G2
   "data15_13TeV:data15_13TeV.00280862.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G3
   "data15_13TeV:data15_13TeV.00280950.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G3
   "data15_13TeV:data15_13TeV.00280977.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G4
   "data15_13TeV:data15_13TeV.00281070.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G4
   "data15_13TeV:data15_13TeV.00281074.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G4
   "data15_13TeV:data15_13TeV.00281075.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #G4
   "data15_13TeV:data15_13TeV.00281317.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #H2
   "data15_13TeV:data15_13TeV.00281385.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #H2
   "data15_13TeV:data15_13TeV.00281411.physics_Main.merge.DAOD_JETM9.f629_m1504_p2425", #H3
   "data15_13TeV:data15_13TeV.00282625.physics_Main.merge.DAOD_JETM9.f640_m1511_p2425", #J1
   "data15_13TeV:data15_13TeV.00282631.physics_Main.merge.DAOD_JETM9.f640_m1511_p2425", #J1
   "data15_13TeV:data15_13TeV.00282712.physics_Main.merge.DAOD_JETM9.f640_m1511_p2425", #J1
   "data15_13TeV:data15_13TeV.00282784.physics_Main.merge.DAOD_JETM9.f640_m1511_p2425", #J2
   "data15_13TeV:data15_13TeV.00282992.physics_Main.merge.DAOD_JETM9.f640_m1511_p2425", #J3
   "data15_13TeV:data15_13TeV.00283074.physics_Main.merge.DAOD_JETM9.f640_m1511_p2425", #J3
   "data15_13TeV:data15_13TeV.00283155.physics_Main.merge.DAOD_JETM9.f640_m1511_p2425", #J3
   "data15_13TeV:data15_13TeV.00283270.physics_Main.merge.DAOD_JETM9.f640_m1511_p2436", #J3
   "data15_13TeV:data15_13TeV.00283429.physics_Main.merge.DAOD_JETM9.f643_m1518_p2436", #J4 new ptag
   "data15_13TeV:data15_13TeV.00283608.physics_Main.merge.DAOD_JETM9.f643_m1518_p2436", #J4
   "data15_13TeV:data15_13TeV.00283780.physics_Main.merge.DAOD_JETM9.f643_m1518_p2436", #J4 new ptag
   "data15_13TeV:data15_13TeV.00284006.physics_Main.merge.DAOD_JETM9.f643_m1518_p2436", #J4
   "data15_13TeV:data15_13TeV.00284154.physics_Main.merge.DAOD_JETM9.f643_m1518_p2436", #J5
   "data15_13TeV:data15_13TeV.00284213.physics_Main.merge.DAOD_JETM9.f643_m1518_p2436", #J6 new ptag
   "data15_13TeV:data15_13TeV.00284285.physics_Main.merge.DAOD_JETM9.f643_m1518_p2425", #J6
   "data15_13TeV:data15_13TeV.00284420.physics_Main.merge.DAOD_JETM9.f643_m1518_p2425", #J6
   "data15_13TeV:data15_13TeV.00284427.physics_Main.merge.DAOD_JETM9.f643_m1518_p2425", #J6
   "data15_13TeV:data15_13TeV.00284484.physics_Main.merge.DAOD_JETM9.f644_m1518_p2425", #J6
]

##################################################################################
#
##################################################################################

#dir = "/afs/cern.ch/user/j/jbossios/work/public/SM/new_xAODJetAnalysis/xAODUnfoldingAnalysis/"
#dir += version
#dir += "/GridSubmissions"
dir = "GridSubmissions"

if JETM9_25ns_MC_Pythia:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.merge.DAOD_JETM9.e3569_s2576_s2132_r6765_r6282_p2375"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.merge.DAOD_JETM9.e3668_s2576_s2132_r6765_r6282_p2375"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.merge.DAOD_JETM9.e3668_s2576_s2132_r6765_r6282_p2375"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.merge.DAOD_JETM9.e3668_s2576_s2132_r6765_r6282_p2375"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.merge.DAOD_JETM9.e3668_s2576_s2132_r6765_r6282_p2375"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.merge.DAOD_JETM9.e3569_s2608_s2183_r6765_r6282_p2375"
    if JZ == "JZ7":
      sample = "mc15_13TeV:mc15_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.merge.DAOD_JETM9.e3668_s2608_s2183_r6765_r6282_p2375"
    if JZ == "JZ8":
      sample = "mc15_13TeV:mc15_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.merge.DAOD_JETM9.e3569_s2576_s2132_r6765_r6282_p2375"
      
    command = "RunRscanAnalysis "
    
    command += "--mc=TRUE "

    if RscanInsitu:
        command += "--rscanInsitu=TRUE "	    

    command += "--jetColl="
    command += JetCollection
    command += " "

    if not SaveRscanTruth:
        command += "--saveRscanTruth=FALSE "
    
    command += "--grid=TRUE "

    # Output dataset
    command += "--output=user."+user+"."

    command += date
    command += "."
    command += version
    command += "."
    command += JZ
        
    # Outputs (local)
    command += " --submitDir=" + dir + "/" + date + "/" + JZ + "_13TeV"

    # Input dataset
    command += " --sample=" + sample

    command += " > logfile_"
    command += JZ
    command += " 2> errors_"
    command += JZ
    command += " &"

    print command
    os.system(command)


if JETM9_25ns_Data:
  for Dataset in JETM9Data:

    # Setting the correct sample
    sample = Dataset

    command = "RunRscanAnalysis "

    command += "--mc=FALSE "

    command += "--jetColl="
    command += JetCollection
    command += " "

    if RscanInsitu:
        command += "--rscanInsitu=TRUE "	    

    if not SaveRscanTruth:
        command += "--saveRscanTruth=FALSE "
    
    command += "--grid=TRUE "

    dataset = Dataset[26:34]
  
    # Output dataset
    command += "--output=user."+user+"."
    command += date
    command += "."
    command += version
    command += "."
    command += dataset

    # Outputs (local)
    command += " --submitDir=" + dir + "/" + date + "/JETM9_"
    command += dataset

    # Input dataset
    command += " --sample=" + sample

    command += " > logfile_"
    command += dataset
    command += " 2> errors_"
    command += dataset

    command += " &"

    print command
    os.system(command)

if JETM9_25ns_MC_Powheg:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.426001.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ1.merge.DAOD_JETM9.e3788_s2608_s2183_r6765_r6282_p2375"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.426002.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ2.merge.DAOD_JETM9.e3788_s2608_s2183_r6765_r6282_p2375"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.426003.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ3.merge.DAOD_JETM9.e3788_s2608_s2183_r6765_r6282_p2375"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.426004.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ4.merge.DAOD_JETM9.e3788_s2608_s2183_r6765_r6282_p2375"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.426005.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ5.merge.DAOD_JETM9.e3788_s2608_s2183_r6765_r6282_p2375"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.426006.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ6.merge.DAOD_JETM9.e3788_s2608_s2183_r6765_r6282_p2375"
    if JZ == "JZ7":
      sample = "mc15_13TeV:mc15_13TeV.426007.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ7.merge.DAOD_JETM9.e3788_s2608_s2183_r6765_r6282_p2375"
    if JZ == "JZ8":
      sample = "mc15_13TeV:mc15_13TeV.426008.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ8.merge.DAOD_JETM9.e3788_s2608_s2183_r6765_r6282_p2375"
    if JZ == "JZ9":
      sample = "mc15_13TeV:mc15_13TeV.426009.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ9.merge.DAOD_JETM9.e3788_s2608_s2183_r6765_r6282_p2375"

      
    command = "RunRscanAnalysis "
    
    command += "--mc=TRUE "

    command += "--jetColl="
    command += JetCollection
    command += " "

    if RscanInsitu:
        command += "--rscanInsitu=TRUE "	    

    if not SaveRscanTruth:
        command += "--saveRscanTruth=FALSE "
    
    command += "--grid=TRUE "

    # Output dataset
    command += "--output=user."+user+"."
    command += date
    command += "."
    command += version
    command += "."
    command += JZ
        
    # Outputs (local)
    command += " --submitDir=" + dir + "/" + date + "/" + JZ + "_13TeV"

    # Input dataset
    command += " --sample=" + sample

    command += " > logfile_"
    command += JZ
    command += " 2> errors_"
    command += JZ
    command += " &"

    print command
    os.system(command)


if JETM9_25ns_MC_Herwig:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.426101.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ1.merge.DAOD_JETM9.e3993_s2608_s2183_r6869_r6282_p2419"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.426102.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ2.merge.DAOD_JETM9.e3993_s2608_s2183_r6869_r6282_p2419"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.426103.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ3.merge.DAOD_JETM9.e3993_s2608_s2183_r6869_r6282_p2419"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.426104.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ4.merge.DAOD_JETM9.e3993_s2608_s2183_r6869_r6282_p2419"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.426105.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ5.merge.DAOD_JETM9.e3993_s2608_s2183_r6869_r6282_p2419"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.426106.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ6.merge.DAOD_JETM9.e3993_s2608_s2183_r6869_r6282_p2419"
    if JZ == "JZ7":
      sample = "mc15_13TeV:mc15_13TeV.426107.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ7.merge.DAOD_JETM9.e3993_s2608_s2183_r6869_r6282_p2419"
    if JZ == "JZ8":
      sample = "mc15_13TeV:mc15_13TeV.426108.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ8.merge.DAOD_JETM9.e3993_s2608_s2183_r6869_r6282_p2419"
    if JZ == "JZ9":
      sample = "mc15_13TeV:mc15_13TeV.426109.PowhegHerwigEvtGen_AUET2_CT10_jetjet_JZ9.merge.DAOD_JETM9.e3993_s2608_s2183_r6869_r6282_p2419"
      
    command = "RunRscanAnalysis "
    
    command += "--mc=TRUE "

    command += "--jetColl="
    command += JetCollection
    command += " "

    if RscanInsitu:
        command += "--rscanInsitu=TRUE "	    

    if not SaveRscanTruth:
        command += "--saveRscanTruth=FALSE "
    
    command += "--grid=TRUE "

    # Output dataset
    command += "--output=user."+user+"."
    command += date
    command += "."
    command += version
    command += "."
    command += JZ
        
    # Outputs (local)
    command += " --submitDir=" + dir + "/" + date + "/" + JZ + "_13TeV"

    # Input dataset
    command += " --sample=" + sample

    command += " > logfile_"
    command += JZ
    command += " 2> errors_"
    command += JZ
    command += " &"

    print command
    os.system(command)


if JETM9_25ns_MC_Pythia_MC15b:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.merge.DAOD_JETM9.e3569_s2576_s2132_r7267_r6282_p2470"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.merge.DAOD_JETM9.e3668_s2576_s2132_r7267_r6282_p2470"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.merge.DAOD_JETM9.e3668_s2576_s2132_r7267_r6282_p2470"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.merge.DAOD_JETM9.e3668_s2576_s2132_r7267_r6282_p2470"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.merge.DAOD_JETM9.e3668_s2576_s2132_r7267_r6282_p2470"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.merge.DAOD_JETM9.e3569_s2608_s2183_r7267_r6282_p2470"
    if JZ == "JZ7":
      sample = "mc15_13TeV:mc15_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.merge.DAOD_JETM9.e3668_s2608_s2183_r7267_r6282_p2470"
    if JZ == "JZ8":
      sample = "mc15_13TeV:mc15_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.merge.DAOD_JETM9.e3569_s2576_s2132_r7267_r6282_p2470"
    if JZ == "JZ9":
      sample = "mc15_13TeV:mc15_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.merge.DAOD_JETM9.e3569_s2576_s2132_r7267_r6282_p2470"

    command = "RunRscanAnalysis "

    command += "--mc=TRUE "

    command += "--jetColl="
    command += JetCollection
    command += " "

    if RscanInsitu:
        command += "--rscanInsitu=TRUE "	    

    if not SaveRscanTruth:
        command += "--saveRscanTruth=FALSE "
   
    command += "--grid=TRUE "

    # Output dataset
    command += "--output=user."+user+"."
    command += date
    command += "."
    command += version
    command += "."
    command += JZ

    # Outputs (local)
    command += " --submitDir=" + dir + "/" + date + "/" + JZ + "_13TeV_MC15b"

    # Input dataset
    command += " --sample=" + sample

    command += " > logfile_PythiaMC15b_"
    command += JZ
    command += " 2> errors_PythiaMC15b_"
    command += JZ
    command += " &"

    print command
    os.system(command)

if JETM9_25ns_MC_Powheg_MC15b:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.426001.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ1.merge.DAOD_JETM9.e3788_s2608_s2183_r7326_r6282_p2561"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.426002.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ2.merge.DAOD_JETM9.e3788_s2608_s2183_r7326_r6282_p2561"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.426003.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ3.merge.DAOD_JETM9.e3788_s2608_s2183_r7326_r6282_p2561"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.426004.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ4.merge.DAOD_JETM9.e3788_s2608_s2183_r7326_r6282_p2561"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.426005.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ5.merge.DAOD_JETM9.e3788_s2608_s2183_r7326_r6282_p2561"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.426006.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ6.merge.DAOD_JETM9.e3788_s2608_s2183_r7326_r6282_p2561"
    if JZ == "JZ7":
      sample = "mc15_13TeV:mc15_13TeV.426007.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ7.merge.DAOD_JETM9.e3788_s2608_s2183_r7326_r6282_p2561"
    if JZ == "JZ8":
      sample = "mc15_13TeV:mc15_13TeV.426008.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ8.merge.DAOD_JETM9.e3788_s2608_s2183_r7326_r6282_p2561"
    if JZ == "JZ9":
      sample = "mc15_13TeV:mc15_13TeV.426009.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ9.merge.DAOD_JETM9.e3788_s2608_s2183_r7326_r6282_p2561"

    command = "RunRscanAnalysis "
    
    command += "--mc=TRUE "

    if not SaveRscanTruth:
        command += "--saveRscanTruth=FALSE "

    command += "--jetColl="
    command += JetCollection
    command += " "

    if RscanInsitu:
        command += "--rscanInsitu=TRUE "	    
    
    command += "--grid=TRUE "

    # Output dataset
    command += "--output=user."+user+"."
    command += date
    command += "."
    command += version
    command += "."
    command += JZ
        
    # Outputs (local)
    command += " --submitDir=" + dir + "/" + date + "/" + JZ + "_13TeV_Powheg_MC15b"

    # Input dataset
    command += " --sample=" + sample

    command += " > logfile_PowhegMC15b_"
    command += JZ
    command += " 2> errors_PowhegMC15b_"
    command += JZ
    command += " &"

    print command
    os.system(command)


if JETM9_25ns_MC_Sherpa_MC15a:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.426131.Sherpa_CT10_jets_JZ1.merge.DAOD_JETM9.e4355_s2608_r6869_r6282_p2559"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.426132.Sherpa_CT10_jets_JZ2.merge.DAOD_JETM9.e4355_s2608_r6869_r6282_p2559"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.426133.Sherpa_CT10_jets_JZ3.merge.DAOD_JETM9.e4355_s2608_r6869_r6282_p2559"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.426134.Sherpa_CT10_jets_JZ4.merge.DAOD_JETM9.e4355_s2608_r6869_r6282_p2559"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.426135.Sherpa_CT10_jets_JZ5.merge.DAOD_JETM9.e4355_s2608_r6869_r6282_p2559"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.426136.Sherpa_CT10_jets_JZ6.merge.DAOD_JETM9.e4355_s2608_r6869_r6282_p2559"

    command = "RunRscanAnalysis "
    
    command += "--mc=TRUE "

    if RscanInsitu:
        command += "--rscanInsitu=TRUE "	    

    if not SaveRscanTruth:
        command += "--saveRscanTruth=FALSE "

    command += "--jetColl="
    command += JetCollection
    command += " "
    
    command += "--grid=TRUE "

    # Output dataset
    command += "--output=user."+user+"."
    command += date
    command += "."
    command += version
    command += "."
    command += JZ
        
    # Outputs (local)
    command += " --submitDir=" + dir + "/" + date + "/" + JZ + "_13TeV_Sherpa_MC15a"

    # Input dataset
    command += " --sample=" + sample

    command += " > logfile_SherpaMC15a_"
    command += JZ
    command += " 2> errors_SherpaMC15a_"
    command += JZ
    command += " &"

    print command
    os.system(command)

