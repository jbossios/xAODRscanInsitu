#!/usr/bin/env python

##******************************************
#downloadAndMerge.py
#download and merge output datasets produced by dijet analysis code on the GRID

#NOTE before starting, set the variables below
#NOTE just run the script without any parameter to get instruction
#HOW TO time python -u downloadAndMerge.py dataset
#NOTE 'dataset' can have wildcards, the matching datasets will be treated independently
#EXAMPLE time python -u downloadAndMerge.py user.gfacini.*.v06_*.root/
##******************************************

#------------------------------------------
#import
import os, sys, subprocess, glob
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--container", dest='container', default="None",
     help="Name of dataset to be downloaded, may include wildcards")
parser.add_argument("--file", dest='fileOfContainers', default="None",
     help="A text file containing names of datasets to be downloaded")
parser.add_argument("--types", dest='types', default="tree",
     help="Comma seperated list of types of datasets to be downloaded")
args = parser.parse_args()

def main():
  ##******************************************
  #NOTE before starting, set the variables
  ##******************************************
  outputPath = 'gridOutput/'
#  datasetVariants = ['cutflow', 'hist-output', 'metadata', 'tree']#, 'log']
  datasetVariants = args.types.split(',')
  mergeRawDatasets = True
  ##******************************************

  #------------------------------------------
  #get current directory
  currentDir = os.getcwd()

  #------------------------------------------
  #if given a wildcarded set of containers
  if not args.container == 'None':
    #prepare raw-dataset string
    rawDataset = args.container
    rawDataset = rawDataset.rstrip('/')
    rawDataset = rawDataset.rstrip('*')
    rawDataset += '*/'

    print rawDataset
    #get list of matching datasets
    (stdout, stderr) = subprocess.Popen(['dq2-ls',rawDataset], stdout=subprocess.PIPE).communicate()
    datasetList = stdout.splitlines()

  #------------------------------------------
  #get containers from a text file
  else:
   datasetList = []
   #if given a text file of containers
   if not args.fileOfContainers == 'None':
     textFileList = [args.fileOfContainers]
   #otherwise search for containers in gridOutput/
   else:
     textFileList = glob.glob("gridOutput/gridJobs/*/outputContainers.txt") #Get lists of the output containers

   for iFile, file in enumerate(textFileList):
     file = open(file, 'r')
     for line in file:
       if not ( line.find("#") == 0 or len(line) < 1 ):
         rawDataset = line.rstrip()
         rawDataset = rawDataset.rstrip('/')
         rawDataset += '*/'
         print rawDataset
         (stdout, stderr) = subprocess.Popen(['dq2-ls',rawDataset], stdout=subprocess.PIPE).communicate()
         datasetList += stdout.splitlines()

  if len(datasetList) == 0:
    print 'ERROR: No datasets matched'
    exit(1)

  datasetList.sort()
  ## choose only file types given by arguement types ##
  datasetList = [ dataset for dataset in datasetList if any(variant in dataset for variant in datasetVariants)]
  print '\nmatching datasets:'
  for dataset in datasetList:
    print ' %s'%dataset

  numDownloads  = str(len(datasetList))

  #------------------------------------------
  #prepare output directory for raw datasets
  if not os.path.exists(outputPath):
    os.mkdir(outputPath)
  if not os.path.exists(outputPath+'/rawDownload/'):
    os.mkdir(outputPath+'/rawDownload/')
  try:
    os.chdir(outputPath+'/rawDownload/')
  except:
    raise SystemExit('\n***EXIT*** couldn\'t change directory')

  #download datasets
  print '\n******************************************\ndownloading datasets'
  for idataset, dataset in enumerate(datasetList):
    print '\n ---------------------- downloading dataset ('+str(idataset)+'/'+numDownloads+'): %s'%dataset
    os.system("dq2-get "+dataset)

    #remove scope from dataset name (i.e. user.x:)
    datasetList[idataset] = dataset.split(':')[1]

  #prepare output directories for merged files
  os.chdir('../')
  for variant in datasetVariants:
    directory = variant+'/'
    if not os.path.exists(directory):
      os.mkdir(directory)

  #merge datasets
  if mergeRawDatasets is True:
    print '\n******************************************\nmerging files'
    for dataset in datasetList:
      if '.log/' in dataset:
        continue
      print '\n dataset: %s'%dataset

      for datasetVariant in datasetVariants:
        if datasetVariant in dataset:
          variant = datasetVariant
          break
      print '  variant: %s'%variant

      outputFileName = variant+'/'+dataset.rstrip('/')
      if not outputFileName.endswith('.root'):
        outputFileName += '.root'
      inputFilesName = 'rawDownload/'+dataset.rstrip('/')+'*/*.root*'
      print '   outputFileName: %s'%outputFileName
      print '   inputFilesName: %s'%inputFilesName
      os.system('hadd '+outputFileName+' '+inputFilesName)

  #------------------------------------------
  #at last, go back to original directory
  os.chdir(currentDir)


if __name__ == "__main__":
  main()
  print '\n\ndone'
